package com.viamgr.androidmaker.mylibrary;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

import com.viamgr.androidmaker.mylibrary.databinding.ActivityTestBinding;
//import com.viamgr.androidmaker.mylibrary.databinding.ActivityMainBinding;


/**
 * Created by moradi on 8/18/2016.
 */
public class BaseActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityTestBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_test);
        User user = new User("Test", "User");
        binding.setUser(user);
    }
}
